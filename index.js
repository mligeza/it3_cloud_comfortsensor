/**
 * COMFORT SENSOR:
 * 
 */
const axios = require('axios')


 //SETTINGS:
 //Gateway1 (GWID 0020)
 var address = "http://127.0.0.1:8000/comfort/state";


 function randomInt(min, max) {
	return min + Math.floor((max - min) * Math.random());
}
function randomFloat(min, max) {
    return min + (max - min) * Math.random();
}

const mock = (csid)=>{
    var tempVal=randomInt(15, 30); // min: 18, max: 28
    var humVal=randomInt(0,100); // 18-20° C względna wilgotność powietrza wynosi 40-50%, //25° C względna wilgotność powietrza wynosi 35-40%.
    var pm10Val=randomFloat(0,55); // stężenie średniodobowe PM10 nie powinno przekraczać niż 50 µg/m³.
    var pm25Val=randomFloat(0,30); // dobowe stężenie PM2,5 nie powinno przekraczać 25 µg/m³ 
    var co2Val=randomInt(0,6000); // max: 5000 średnioważone 8h
    var vocVal=randomInt(0,350) // powyzej 300 to źle - brak formalnych norm
    

    axios
    .post(address, {
      temperature: tempVal,
      humidity: humVal,
      pm10:pm10Val,
      pm25:pm25Val,
      co2:co2Val,
      voc:vocVal,
      CSID:csid,
      timestamp:Date.now()
    })
    .then(res => {
      //console.log(res.status + ": "+ res.data)
      ////console.log(res.data)
    })
    .catch(error => {
      console.error(error)
    })
    
};

//console.log("Sensor started!");

//send state each minute

//moch for Hall1 (GWID 0020)
setInterval( 
    ()=>{mock(23)}, 60000);

setInterval( 
    ()=>{mock(24)}, 62500);

setInterval( 
    ()=>{mock(33)}, 65000);

setInterval( 
    ()=>{mock(34)}, 67500);